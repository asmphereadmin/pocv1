package StepDefination;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Loginpage {
	
	public  WebDriver driver;
	
	@Before("env")
	@Given ("^open chrome browser and launch SportsBook Desktop Application$")
	public void luanchapp(){
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//TestConfig//chromedriver_win32_2.33.exe");
		driver = new ChromeDriver();
		driver.get("http://sports.ladbrokes.com");
	}
	
	@And ("^Check the URL$")
	public void checkurl(){
		
		String URL = driver.getCurrentUrl();
		System.out.println(URL);
	}
	
	 @When ("^user navigates to footballpage$")
public void navigatetofootballpage(){
	
	driver.navigate().to("https://sports.ladbrokes.com/en-gb/betting/football/");
}
	 
	 @Then ("^user enters username and password$")
	 public void entercredentails(DataTable usercredentials) throws InterruptedException{
		 driver.findElement(By.xpath("//*[@id='username']")).sendKeys("username");
		 driver.findElement(By.xpath("//*[@id='password']")).sendKeys("password");
		 Thread.sleep(4000);
	 }
	 @And ("^click on login$")
	 public void clicklogin() throws InterruptedException{
		 
		 driver.findElement(By.xpath("//*[@id='loginSubmit']/div[3]/button[1]")).click();
		 Thread.sleep(4000);
	 }
}
